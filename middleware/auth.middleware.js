const passport = require('passport');
const { APP_STATE, DSCRONKEY } = require('../lib/utility/util_keys');

const verifyCallback = (req, resolve, reject) => async (err, user, info) => {
    if (APP_STATE !== 'local')
        if (err || info || !user) {
            return reject({
                status_code: 401,
                status: false,
                message: 'Please authenticate',
                data: 'Authentication code not matching',
            });
        }
    req.user = user;
    resolve();
};

const auth = async (req, res, next) => {
    return new Promise((resolve, reject) => {
        passport.authenticate('jwt', { session: false }, verifyCallback(req, resolve, reject))(
            req,
            res,
            next,
        );
    })
        .then(() => next())
        .catch((err) => res.status(401).send(err));
    // .catch((err) => next(err));
};

module.exports = { auth };
