const Joi = require('joi');
const constant = require('../../utility/constants');
// create login schema
function loginBodySchema() {
    const schema = {
        body: Joi.object().keys({
            email: Joi.string()
                .email({ minDomainSegments: 2, tlds: { allow: ['com', 'net'] } })
                .required(),
            password: Joi.string().required(),
            user_type: Joi.string().optional(),
            fcm_token: Joi.string(),
            device_type: Joi.string()
                .valid(constant.IOS, constant.ANDROID, constant.WEB)
                .required(),
        }),
    };
    return schema;
}

function authLoginBodySchema() {
    const schema = {
        body: Joi.object().keys({
            email: Joi.string()
                .email({ minDomainSegments: 2, tlds: { allow: ['com', 'net'] } })
                .required(),
            password: Joi.string().required(),
            user_type: Joi.string().optional(),
            device_type: Joi.string()
                .valid(constant.IOS, constant.ANDROID, constant.WEB)
                .required(),
        }),
    };
    return schema;
}

function loggedInBodySchema() {
    const schema = {
        body: Joi.object().keys({
            _id: Joi.string().length(24).required(),
            user_type: Joi.string().optional(),
            fcm_token: Joi.string(),
            device_type: Joi.string()
                .valid(constant.IOS, constant.ANDROID, constant.WEB)
                .required(),
        }),
    };
    return schema;
}
// create token schema
function tokenBodySchema() {
    const schema = {
        body: Joi.object().keys({
            _id: Joi.string().length(24).required(),
            fcm_token: Joi.string().required(),
            device_type: Joi.string().required(),
        }),
    };
    return schema;
}
// create otp schema
function otpBodySchema() {
    const schema = {
        body: Joi.object().keys({
            _id: Joi.string().length(24).required(),
            otp: Joi.string().length(4).regex(/^\d+$/).required(),
        }),
    };
    return schema;
}

// create setPassword schema
function setPasswordBodySchema() {
    const schema = {
        body: Joi.object().keys({
            _id: Joi.string().length(24).required(),
            new_password: Joi.string().required(),
        }),
    };
    return schema;
}
// create faceVerify schema
function faceVerifyBodySchema() {
    const schema = {
        body: Joi.object().keys({
            email: Joi.string()
                .email({ minDomainSegments: 2, tlds: { allow: ['com', 'net'] } })
                .required(),
            face: Joi.string().required(),
        }),
    };
    return schema;
}

// get notification schema
function getNotificationBodySchema() {
    const schema = {
        params: {
            id: Joi.string().length(24),
        },
    };
    return schema;
}
// logout schema
function logoutBodySchema() {
    const schema = {
        body: Joi.object().keys({
            id: Joi.string().length(24),
            device_type: Joi.string(),
        }),
    };
    return schema;
}
// logout schema
function fcmUpdateBySchema() {
    const schema = {
        body: Joi.object().keys({
            fcm_token: Joi.string().required(),
            device_type: Joi.string().required(),
        }),
        params: {
            id: Joi.string().length(24).required(),
        },
    };
    return schema;
}
module.exports = {
    loginSchema: loginBodySchema(),
    authLoginSchema: authLoginBodySchema(),
    authLoggedInSchema: loggedInBodySchema(),
    tokenSchema: tokenBodySchema(),
    otpSchema: otpBodySchema(),
    setPasswordSchema: setPasswordBodySchema(),
    faceVerifySchema: faceVerifyBodySchema(),
    logoutSchema: logoutBodySchema(),
    fcmUpdateBySchema: fcmUpdateBySchema(),
};
