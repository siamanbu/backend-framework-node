const router = require('express').Router();

const UserRoute = require('./user/user.route');

// router.get('/auth', (req, res) => {
//   res.status(200).json({ message: 'Authenticated user' });
// });

router.get('/', (req, res) => {
    res.render('index', {
        title: 'Welcome to  API V1',
    });
});

router.use('/user', UserRoute);

module.exports = router;
